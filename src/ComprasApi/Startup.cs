﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using ComprasApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace ComprasApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
               {
                   c.SwaggerDoc("v1", new Info
                   {
                       Version = "v1",
                       Title = "WebApi ComprasApi Swagger Test",
                       Description = "First WebApi with Swagger",
                       TermsOfService = "None",
                       Contact = new Contact
                       {
                           Name = "Welerson Freitas",
                           Email = "welersonfgoliveira@hotmail.com",
                           Url = "linkedin.com/in/welerson-freitas-69b80b165"
                       }
                   });
                   // Set the comments path for the Swagger JSON and UI.
                   //  var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                   // var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                   //  c.IncludeXmlComments(xmlPath);
               });

            //services.AddDbContext<ListContext>(opt => opt.UseInMemoryDatabase("ItemsList"));

            string connectionString = Configuration.GetConnectionString("DbList");

            services.AddDbContext<ListContext>(opt => opt.UseSqlServer(connectionString));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Add Swagger/SwaggerUI to pipeline
            app.UseSwagger(c =>
                {
                    c.PreSerializeFilters.Add((document, request) =>
                    {
                        document.Paths = document.Paths.ToDictionary(p => p.Key.ToLowerInvariant(), p => p.Value);
                    });
                }).UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebApi Compras Swagger API v1");
                    c.RoutePrefix = string.Empty;
                });

            app.UseMvc();
        }
    }
}
