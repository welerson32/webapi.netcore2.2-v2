using Microsoft.EntityFrameworkCore;

namespace ComprasApi.Models
{
    public class ListContext : DbContext
    {
        public ListContext(DbContextOptions<ListContext> options) : base(options)
        {
        }

        public DbSet<ItemsList> List { get; set; }

    }
}
