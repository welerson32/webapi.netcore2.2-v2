
using ComprasApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ComprasApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ListController : ControllerBase
    {
        private readonly ListContext _context;
        public ListController(ListContext context)
        {
            _context = context;

            if (_context.List.Count() == 0)
            {
                _context.List.Add(new ItemsList { Item = "Arroz, Feijao, Açucar, Produtos de Limpeza" });
                _context.SaveChanges();
            }

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ItemsList>>> GetList()
        {
            return await _context.List.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ItemsList>> GetListById(long id)
        {
            var item = await _context.List.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public async Task<ActionResult<ItemsList>> PostList(ItemsList list)
        {
            _context.List.Add(list);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(PostList), new { id = list.Id }, list);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutList(long id, ItemsList item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteList(long id)
        {
            var item = await _context.List.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.List.Remove(item);
            await _context.SaveChangesAsync();

            return NoContent();

        }

    }
}
